//
//  MapViewController.swift
//  RealHouseLookUp
//
//  Created by Ravi on 25/04/15.
//  Copyright (c) 2015 Ravi. All rights reserved.
//

import UIKit

protocol MapViewControllerDelegate {
//    func showCalendarImage()
}


class MapViewController: UIViewController {
    
     var delegate:MapViewControllerDelegate?

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        var camera = GMSCameraPosition.cameraWithLatitude(12.971940,
            longitude: 77.5936900, zoom: 6)
        var mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        mapView.myLocationEnabled = true
        self.view = mapView
        
        var marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(12.971940, 77.5936900)
        marker.title = "Bangalore"
        marker.snippet = "India"
        marker.map = mapView
        // Do any additional setup after loading the view.
    }

    
    func updateMapView(position:CLLocationCoordinate2D?, title:NSString? , snippet: NSString?) {
        
        var camera = GMSCameraPosition.cameraWithLatitude(17.3840500,
            longitude: 78.4563600, zoom: 3)
        var mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        mapView.myLocationEnabled = true
        self.view = mapView
        
        var marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(17.3840500, 78.4563600)
        marker.title = "Hyderabad"
        marker.snippet = "India"
        marker.map = mapView
  
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
