//
//  ImagesListViewController.swift
//  RealHouseLookUp
//
//  Created by Ravi on 25/04/15.
//  Copyright (c) 2015 Ravi. All rights reserved.
//

import UIKit

class ImagesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var imagesTableView: UITableView?
    
    var imageItemsArray: NSArray = ["img1","img10", "img2","img3","img4","img5","img6","img7","img8","img9"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagesTableView!.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageItemsArray.count
        
    }
    
    func numberOfSections() -> Int {
        
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell! = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
        var imageView = UIImageView(frame: CGRectMake(4, 5, 240, 145));
        var imageName  = imageItemsArray [indexPath.row] as NSString
        var image = UIImage(named: imageName)
        imageView.image = image;
//        imageView.layer.cornerRadius = 35.0;
//        imageView.layer.masksToBounds = true;
        cell.addSubview(imageView);
        
        var lblName = UILabel(frame: CGRectMake(0, 150, 240, 40));
        lblName.font = UIFont(name:"HelveticaNeue", size: 17);
        lblName.textColor = UIColor(red: 133.0/255.0, green: 117.0/255.0, blue: 100.0/255.0, alpha: 1);
        lblName.text = "Ravi";
        lblName.adjustsFontSizeToFitWidth = true
        cell.addSubview(lblName);
        
       return cell

    }
    
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 180
    }
   
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        <#code#>
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
