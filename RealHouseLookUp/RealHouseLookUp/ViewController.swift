//
//  ViewController.swift
//  RealHouseLookUp
//
//  Created by Ravi on 25/04/15.
//  Copyright (c) 2015 Ravi. All rights reserved.
//

import UIKit

class ViewController: UIViewController , MapViewControllerDelegate, UISearchBarDelegate,UISearchDisplayDelegate {

    
    @IBOutlet weak var buttonsContainerView :UIView?
    @IBOutlet weak var mapcontainerView: UIView?
    weak var mapViewController: MapViewController?
    @IBOutlet weak var areaSearchBar: UISearchBar?
    @IBOutlet weak var citySearchBar: UISearchBar?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
        
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "MapViewController" {
           
            self.mapViewController = segue.destinationViewController as? MapViewController

        }

    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        
        self.mapViewController?.updateMapView(nil, title: nil, snippet: nil)
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

